﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngematicaAppTest.Model;

namespace IngematicaAppTest.Business
{
    public partial class TipodeViajeBusiness
    {

        public List<TipoViajeModel> GettipoViaje()
        {
            List<TipoViajeModel> Listaviaje = CargarLista();

            return Listaviaje;
        }
        public TipoViajeModel obtenerId(int idtipo)
        {
            List<TipoViajeModel> Listaviaje = CargarLista();

            TipoViajeModel viaje = Listaviaje.Where(x => x.idtipo== idtipo).FirstOrDefault();

            return viaje;
        }
        private static List<TipoViajeModel> CargarLista()
        {
            List<TipoViajeModel> Listaviaje = new List<TipoViajeModel>();
            Listaviaje.Add(new TipoViajeModel { idtipo = 0, Tipo = "--Seleccionar--" });
            Listaviaje.Add(new TipoViajeModel { idtipo = 1, Tipo = "RUTA" });
            Listaviaje.Add(new TipoViajeModel { idtipo = 2, Tipo = "AUTOPISTA" });
            return Listaviaje;

        }

    }
}
