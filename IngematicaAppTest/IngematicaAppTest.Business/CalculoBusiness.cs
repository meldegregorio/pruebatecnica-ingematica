﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngematicaAppTest.Business
{
    public partial class CalculoBusiness
    {
        public double calcular( int dias, int tipoViaje, int transporte)
        {
           double  demora=0;
            if (tipoViaje == 1)
            {
                demora=(dias+transporte)*1.10;
            }
            else
            {
                demora = dias+transporte;
            }
            return demora;
        }

    }
}
