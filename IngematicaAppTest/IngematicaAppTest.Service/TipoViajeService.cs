﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngematicaAppTest.Model;
using IngematicaAppTest.Business;

namespace IngematicaAppTest.Service
{
    public partial class TipoViajeService
    {
        public List<TipoViajeModel> GettipoViaje()
        {
            TipodeViajeBusiness bs = new TipodeViajeBusiness();

            return bs.GettipoViaje();
        }

        public TipoViajeModel obtenerId(int id)
        {
            TipodeViajeBusiness bs = new TipodeViajeBusiness();

            TipoViajeModel tipoviaje = bs.obtenerId(id);

            return tipoviaje;
        }
    }
}
