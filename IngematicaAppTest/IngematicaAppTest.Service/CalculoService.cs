﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngematicaAppTest.Model;
using IngematicaAppTest.Business;

namespace IngematicaAppTest.Service
{
    public partial class CalculoService
    {

        public double calculo(int dias, int idviaje, int transporte)
        {
            CalculoBusiness bs = new CalculoBusiness();
           

            double tipoviaje = bs.calcular(dias, idviaje, transporte);

            return tipoviaje;
        }
    }
}
