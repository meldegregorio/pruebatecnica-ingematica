﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IngematicaAppTest.Model;
using IngematicaAppTest.Service;

namespace IngematicaAppTest
{
    public partial class FrmTest : Form
    {
      
        public FrmTest()
        {
            InitializeComponent();

            InitializeCombos();
            
        }


        private void InitializeCombos()
        {
            InitializeComboTipoTransporte();

            InitializeCoboLocalidad();

            InitializeCoboTipoViaje();
           

        }

        int x = 0;
        int t = 0;
        int p = 0;
        private void InitializeComboTipoTransporte()
        {
            List<TipoTransporteModel> tipoTransporteList = new List<TipoTransporteModel>();
            TipoTransporteService tipoTransporteService = new TipoTransporteService();
            tipoTransporteList = tipoTransporteService.GetList();

            var bindingSourceTipoTransporte = new BindingSource();
            bindingSourceTipoTransporte.DataSource = tipoTransporteList;

            cbTipoTransporte.DataSource = bindingSourceTipoTransporte;
            cbTipoTransporte.DisplayMember = "Nombre";
            cbTipoTransporte.ValueMember = "Id";
        }

        private void InitializeCoboLocalidad()
        {
            List<LocalidadModel> localidadList = new List<LocalidadModel>();
            LocalidadService localidadService = new LocalidadService();
            localidadList = localidadService.GetList();

            var bindingSourceLocalidad = new BindingSource();
            bindingSourceLocalidad.DataSource = localidadList;

            cbLocalidadDestino.DataSource = bindingSourceLocalidad;
            cbLocalidadDestino.DisplayMember = "Nombre";
            cbLocalidadDestino.ValueMember = "Id";
        }
        private void InitializeCoboTipoViaje()
        {
            List<TipoViajeModel> viaje = new List<TipoViajeModel>();
            TipoViajeService tipoViajeService = new TipoViajeService();
            viaje = tipoViajeService.GettipoViaje();

            var bindingSourceViaje = new BindingSource();
            bindingSourceViaje.DataSource = viaje;

            CB_Tipoviaje.DataSource = bindingSourceViaje;
            CB_Tipoviaje.DisplayMember = "Tipo";
            CB_Tipoviaje.ValueMember = "IdTipo";
        }
   
        private void cbLocalidadDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (cbLocalidadDestino.SelectedItem == null) return;
            var v = (LocalidadModel)cbLocalidadDestino.SelectedItem;
            if (v != null)
            {
               x = Convert.ToInt32( v.DiasDemora);

            }
           
        }

        private void CB_Tipoviaje_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_Tipoviaje.SelectedItem == null) return;
            var v = (TipoViajeModel)CB_Tipoviaje.SelectedItem;
            if (v != null)
            {
                t = Convert.ToInt32(v.idtipo);

            }
        }
        private void cbTipoTransporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTipoTransporte.SelectedItem == null) return;
            var v = (TipoTransporteModel)cbTipoTransporte.SelectedItem;
            if (v != null)
            {
                p = Convert.ToInt32(v.CoeficineteDemora);

            }
        }


        private void btnCalcular_Click(object sender, EventArgs e)
        {
            CalculoService calcular = new CalculoService();

            double demora = Math.Round(calcular.calculo(x, t, p));
            txtDiasDemora.Text = demora.ToString();

            txtFechaLlegada.Text = dtpFechaInicial.Value.Date.AddDays(demora).ToShortDateString();


        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cbLocalidadDestino.SelectedIndex = 0;
            cbTipoTransporte.SelectedIndex = 0;
            CB_Tipoviaje.SelectedIndex = 0;
            dtpFechaInicial.Value = DateTime.Today;
            txtDiasDemora.Text = "";
            txtFechaLlegada.Text = "";
        }

        private void dtpFechaInicial_ValueChanged(object sender, EventArgs e)
        {
             var dtp = sender as DateTimePicker;
             if (dtp.Value.DayOfWeek == DayOfWeek.Saturday || dtp.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    MessageBox.Show("No es posible seleccionar el día sábado o domingo");
                    
                    dtp.Value = dtp.Value.AddDays(dtp.Value.DayOfWeek == DayOfWeek.Sunday ? -2 : -1);
                    dtp.Focus();
                }
           
        }
    }


}
