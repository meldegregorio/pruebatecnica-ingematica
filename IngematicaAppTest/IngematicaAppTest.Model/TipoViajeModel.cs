﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngematicaAppTest.Model
{
    public partial class TipoViajeModel
    {
        public int idtipo { get; set; }

        public string Tipo { get; set; }
    }
}
