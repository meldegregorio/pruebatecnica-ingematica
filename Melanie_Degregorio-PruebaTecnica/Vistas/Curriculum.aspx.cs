﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Vistas
{
    public partial class Curriculum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Iblinkedin_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("https://www.linkedin.com/in/melanie-degregorio-marrero-9750311a0/");
        }

        protected void IBgithub_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("https://github.com/MelanieDegregorio23");
        }

        protected void IBGitlab_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("https://gitlab.com/meldegregorio");
        }
    }
}