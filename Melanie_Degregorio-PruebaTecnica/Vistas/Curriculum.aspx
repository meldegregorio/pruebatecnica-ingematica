﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Curriculum.aspx.cs" Inherits="Vistas.Curriculum" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 525px;
            margin-left: 40px;
        }
        .auto-style2 {
            width: 100%;
        }
        .auto-style4 {
            width: 22px;
        }
        .auto-style5 {
            width: 208px;
        }
        .auto-style6 {
            margin-left: 69px;
        }
        .auto-style11 {
            margin-left: 4px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .auto-style14 {
            margin-left: 1px;
        }
        .auto-style18 {
            width: 258px;
        }
        .auto-style20 {
            margin-left: 3px;
        }
        .auto-style21 {
            margin-left: 53px;
        }
        .auto-style22 {
            margin-left: 54px;
        }
        .auto-style24 {
            width: 258px;
            height: 28px;
        }
        .auto-style26 {
            width: 208px;
            height: 28px;
        }
        .auto-style27 {
            width: 22px;
            height: 28px;
        }
        .auto-style28 {
            height: 28px;
        }
        .auto-style29 {
            margin-left: 0px;
            margin-top: 0px;
            margin-bottom: 70px;
        }
        .auto-style33 {
            width: 258px;
            height: 145px;
        }
        .auto-style35 {
            width: 208px;
            height: 145px;
        }
        .auto-style36 {
            width: 22px;
            height: 145px;
        }
        .auto-style37 {
            height: 145px;
        }
        .auto-style47 {
            width: 258px;
            height: 23px;
        }
        .auto-style49 {
            width: 208px;
            height: 23px;
        }
        .auto-style50 {
            width: 22px;
            height: 23px;
        }
        .auto-style51 {
            height: 23px;
        }
        .auto-style52 {
            width: 258px;
            height: 15px;
        }
        .auto-style54 {
            width: 208px;
            height: 15px;
        }
        .auto-style55 {
            width: 22px;
            height: 15px;
        }
        .auto-style56 {
            height: 15px;
        }
        .auto-style62 {
            width: 258px;
            height: 5px;
        }
        .auto-style64 {
            width: 208px;
            height: 5px;
        }
        .auto-style65 {
            width: 22px;
            height: 5px;
        }
        .auto-style66 {
            height: 5px;
        }
        .auto-style67 {
            width: 258px;
            height: 3px;
        }
        .auto-style69 {
            width: 208px;
            height: 3px;
        }
        .auto-style70 {
            width: 22px;
            height: 3px;
        }
        .auto-style71 {
            height: 3px;
        }
        .auto-style72 {
            margin-bottom: 0px;
        }
        .auto-style73 {
            width: 398px;
        }
        .auto-style74 {
            width: 398px;
            height: 28px;
        }
        .auto-style75 {
            width: 398px;
            height: 145px;
        }
        .auto-style76 {
            width: 398px;
            height: 5px;
        }
        .auto-style77 {
            width: 398px;
            height: 3px;
        }
        .auto-style78 {
            width: 398px;
            height: 23px;
        }
        .auto-style79 {
            width: 398px;
            height: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
            <table class="auto-style2">
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Overline="False" Font-Size="XX-Large" Font-Strikeout="False" Font-Underline="True" Text="Melanie Degregorio"></asp:Label>
                    </td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Desarrolladora .Net"></asp:Label>
                    </td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style24">
                        <asp:Label ID="Label3" runat="server" Font-Size="Large" Text="P E R F I L"></asp:Label>
                    </td>
                    <td class="auto-style74"></td>
                    <td class="auto-style26">
                        <asp:Label ID="Label9" runat="server" Font-Size="Large" Text="H A B I L I D A D E S"></asp:Label>
                    </td>
                    <td class="auto-style27"></td>
                    <td class="auto-style28"></td>
                </tr>
                <tr>
                    <td class="auto-style33">Soy estudiante de la Tecnicatura Universitaria en Programación, también hice cursos de front-end. Me considero una persona responsable y ordenada. Buscando mi primera experiencia laboral en el mundo de la programación.</td>
                    <td class="auto-style75">
                        <asp:Image ID="Image1" runat="server" BorderWidth="0px" CssClass="auto-style6" Height="120px" ImageUrl="~/img/mel - copia.png" Width="120px" />
                    </td>
                    <td class="auto-style35">
                        &nbsp;
                        &nbsp;
                        <asp:BulletedList ID="BulletedList1" runat="server" CssClass="auto-style29" Height="81px">
                            <asp:ListItem>Comunicación</asp:ListItem>
                            <asp:ListItem>Responsabilidad</asp:ListItem>
                            <asp:ListItem>Gestión de crisis</asp:ListItem>
                            <asp:ListItem> Trabajo en equipo</asp:ListItem>
                        </asp:BulletedList>
                    </td>
                    <td class="auto-style36"></td>
                    <td class="auto-style37"></td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style62">
                        <asp:Label ID="Label17" runat="server" Font-Size="Large" Text="T E C N O L O G I A S"></asp:Label>
                    </td>
                    <td class="auto-style76">
                                                &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label10" runat="server" Text="E D U C A C I Ó N" Font-Size="Large"></asp:Label>
                    </td>
                    <td class="auto-style64">
                        <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Size="Large" Text="C O N T A C T O"></asp:Label>
                    </td>
                    <td class="auto-style65"></td>
                    <td class="auto-style66"></td>
                </tr>
                <tr>
                    <td class="auto-style67">
                        <asp:BulletedList ID="BulletedList2" runat="server" Height="16px" Width="148px" CssClass="auto-style72">
                            <asp:ListItem>C++</asp:ListItem>
                            <asp:ListItem>C#</asp:ListItem>
                            <asp:ListItem>JAVASCRIPT</asp:ListItem>
                            <asp:ListItem>.NET FRAMEWORK</asp:ListItem>
                            <asp:ListItem>ASP.NET</asp:ListItem>
                            <asp:ListItem>SQL SERVER</asp:ListItem>
                            <asp:ListItem>HTML</asp:ListItem>
                            <asp:ListItem>CSS</asp:ListItem>
                            <asp:ListItem>SASS</asp:ListItem>
                            <asp:ListItem>WEBPACK</asp:ListItem>
                            <asp:ListItem>GIT</asp:ListItem>
                            <asp:ListItem>FIGMA</asp:ListItem>
                        </asp:BulletedList>
                    </td>
                    <td class="auto-style77">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Black" Text="Bachiller en Economía y administración"></asp:Label>
                    </td>
                    <td class="auto-style69">
                        <asp:Label ID="Label5" runat="server" Text="+5491135705316"></asp:Label>
                    </td>
                    <td class="auto-style70">
                        <asp:Image ID="Image2" runat="server" CssClass="auto-style11" Height="25px" ImageUrl="~/img/llamada-telefonica.png" Width="25px" />
                    </td>
                    <td class="auto-style71"></td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label12" runat="server" Font-Size="Medium" ForeColor="#999999" Text="2009-2017 Secundaria 7, Luis Piedrabuena "></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="Label6" runat="server" Text="Rincon de Milberg"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:Image ID="Image3" runat="server" CssClass="auto-style20" Height="25px" ImageUrl="~/img/marcador-de-posicion.png" Width="25px" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Black" Text="Técnico Universitario en programación"></asp:Label>
                    </td>
                    <td class="auto-style5">Degregoriomelanie@gmail.com</td>
                    <td class="auto-style4">
                        <asp:Image ID="Image4" runat="server" CssClass="auto-style20" Height="25px" ImageUrl="~/img/email_negro (1).png" Width="25px" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label14" runat="server" ForeColor="#999999" Text="Abril 2021- Actualmente"></asp:Label>
                    </td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label15" runat="server" ForeColor="#999999" Text="Cursando el último año."></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="Label8" runat="server" Font-Size="Medium" Text="R E D E S  S O C I A L E S"></asp:Label>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <asp:Label ID="Label16" runat="server" ForeColor="#999999" Text="Universidad Tecnológica Nacional"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:ImageButton ID="Iblinkedin" runat="server" CssClass="auto-style14" Height="30px" ImageUrl="~/img/linkedinN.png" OnClick="Iblinkedin_Click" Width="30px" />
                        <asp:ImageButton ID="IBgithub" runat="server" CssClass="auto-style22" Height="30px" ImageUrl="~/img/githubN.png" OnClick="IBgithub_Click" Width="30px" />
                        <asp:ImageButton ID="IBGitlab" runat="server" CssClass="auto-style21" Height="30px" ImageUrl="~/img/gitlab.png" OnClick="IBGitlab_Click" Width="30px" />
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style47"></td>
                    <td class="auto-style78"></td>
                    <td class="auto-style49"></td>
                    <td class="auto-style50"></td>
                    <td class="auto-style51"></td>
                </tr>
                <tr>
                    <td class="auto-style52">
                        <asp:Label ID="Label18" runat="server" Font-Size="Large" Text="P R O Y E C T OS"></asp:Label>
                    </td>
                    <td class="auto-style79">&nbsp;</td>
                    <td class="auto-style54">
                        <asp:Label ID="Label24" runat="server" Font-Size="Large" Font-Strikeout="False" Text="M A S   S O B R E   M I"></asp:Label>
                    </td>
                    <td class="auto-style55"></td>
                    <td class="auto-style56"></td>
                </tr>
                <tr>
                    <td class="auto-style18">
                        <asp:Label ID="Label19" runat="server" Font-Size="Medium" ForeColor="#333333" Text="SISTEMA DE SELECCIÓN DE CANDIDATOS | ACADEMY"></asp:Label>
                    </td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="https://www.youtube.com/watch?v=q489HQ7Dexs">Hincha fanatica </asp:HyperLink>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">
                        <asp:Label ID="Label20" runat="server" Font-Size="Medium" ForeColor="#999999" Text="Abril. 2022 - Julio. 2022"></asp:Label>
                    </td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="https://www.youtube.com/watch?v=Q_Ytf1qGCTM">Pelicula favorita de terror</asp:HyperLink>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style47">
                        <asp:Label ID="Label21" runat="server" Font-Size="Medium" ForeColor="#333333" Text="SISTEMA DE GESTIÓN PARA CLUB DEPORTIVO | UTN FRGP"></asp:Label>
                    </td>
                    <td class="auto-style78">&nbsp;</td>
                    <td class="auto-style49">
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="https://www.youtube.com/watch?v=RdIb85UHfSc">Pelicula favorita de Disney</asp:HyperLink>
                    </td>
                    <td class="auto-style50"></td>
                    <td class="auto-style51"></td>
                </tr>
                <tr>
                    <td class="auto-style18">
                        <asp:Label ID="Label22" runat="server" Font-Size="Medium" ForeColor="#666666" Text="Abril. 2022 - Julio. 2022"></asp:Label>
                    </td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="https://www.youtube.com/watch?v=zAcc07KFCqI">Pelicula favorita animada</asp:HyperLink>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style47">
                        <asp:Label ID="Label23" runat="server" Font-Size="Medium" ForeColor="#333333" Text="SISISTEMA DE  GESTIÓN  DE TURNOS PARA UNA CLINICA | UTN FRGP"></asp:Label>
                    </td>
                    <td class="auto-style78">&nbsp;</td>
                    <td class="auto-style49">
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="https://www.youtube.com/watch?v=YgHPBwLu0BA">Anime favorito</asp:HyperLink>
                    </td>
                    <td class="auto-style50"></td>
                    <td class="auto-style51"></td>
                </tr>
                <tr>
                    <td class="auto-style18">
                        <asp:Label runat="server" Font-Size="Medium" ForeColor="#666666" Text="Noviembre. 2022"></asp:Label>
                    </td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Contacto.aspx">Contactar</asp:HyperLink>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style18">&nbsp;</td>
                    <td class="auto-style73">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
